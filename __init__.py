# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import payment


def register():
    Pool.register(
        payment.Journal,
        payment.Group,
        payment.Payment,
        module='account_payment_confirming_sfc', type_='model')
