=============================
Confirming Payment Scenario
=============================

Imports::
    >>> import os
    >>> import io
    >>> import datetime
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences

Install account_payment and account_invoice::

    >>> config = activate_modules('account_payment_confirming_sfc')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company, today=datetime.datetime(2018, 7, 1)))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> payable = accounts['payable']
    >>> receivable = accounts['receivable']
    >>> expense = accounts['expense']
    >>> Journal = Model.get('account.journal')

Add bank to company::

    >>> Party = Model.get('party.party')
    >>> Bank = Model.get('bank')
    >>> BankAccount = Model.get('bank.account')
    >>> party = Party(name='Bank')
    >>> party.save()
    >>> bank = Bank(party=party)
    >>> bank.save()
    >>> bank_account = BankAccount(bank=bank)
    >>> bank_account.owners.append(company.party)
    >>> iban_company = bank_account.numbers.new()
    >>> iban_company.type = 'iban'
    >>> iban_company.number = 'BE82068896274468'
    >>> bank_account.save()
    >>> iban_company = bank_account.numbers[0]

Create payment journal::

    >>> PaymentJournal = Model.get('account.payment.journal')
    >>> PaymentType = Model.get('account.payment.type')
    >>> ptype = PaymentType(kind='payable', name='Type 1')
    >>> ptype.save()
    >>> payment_journal = PaymentJournal(name='Confirming',
    ...     process_method='confirming')
    >>> payment_journal.party = company.party
    >>> payment_journal.payment_type = ptype
    >>> payment_journal.confirming_bank_account_number = iban_company
    >>> payment_journal.confirming_payable_flavor = 'sfc53'
    >>> payment_journal.confirming_field_set != None
    True
    >>> len(payment_journal.confirming_fields)
    5
    >>> payment_journal.save()

Create parties::

    >>> party = Party(name='Party 1')
    >>> party.save()
    >>> bank_account = BankAccount(bank=bank)
    >>> bank_account.owners.append(party)
    >>> iban = bank_account.numbers.new()
    >>> iban.type = 'iban'
    >>> iban.number = 'DE12500105170648489890'
    >>> bank_account.save()
    >>> party = Party(party.id)
    >>> party2 = Party(name='John Smith Jackson')
    >>> party2.save()
    >>> bank_account = BankAccount(bank=bank)
    >>> bank_account.owners.append(party2)
    >>> iban = bank_account.numbers.new()
    >>> iban.type = 'iban'
    >>> iban.number = 'ES7921000813610123456789'
    >>> bank_account.save()
    >>> party2 = Party(party2.id)

Create invoices::

    >>> PaymentTerm = Model.get('account.invoice.payment_term')
    >>> term = PaymentTerm(name='10D')
    >>> term_line = term.lines.new()
    >>> term_line.type = 'remainder'
    >>> term_line_delta = term_line.relativedeltas.new()
    >>> term_line_delta.days = 10
    >>> term.save()
    >>> Invoice = Model.get('account.invoice')
    >>> invoice = Invoice(type='in')
    >>> invoice.party = party
    >>> invoice.reference = 'N17'
    >>> invoice.payment_term = term
    >>> invoice.invoice_date = datetime.datetime(2018, 6, 18)
    >>> line = invoice.lines.new()
    >>> line.description = 'Description'
    >>> line.account = expense
    >>> line.quantity = 1
    >>> line.unit_price = Decimal('100')
    >>> invoice.click('post')
    >>> line_to_pay, = invoice.lines_to_pay
    >>> invoice2 = Invoice(type='in')
    >>> invoice2.party = party2
    >>> invoice.payment_term = term
    >>> invoice2.reference = 'R-5'
    >>> invoice2.invoice_date = datetime.datetime(2018, 5, 25)
    >>> line = invoice2.lines.new()
    >>> line.description = 'Description'
    >>> line.account = expense
    >>> line.quantity = 1
    >>> line.unit_price = Decimal('180')
    >>> invoice2.click('post')
    >>> line_to_pay2, = invoice2.lines_to_pay

Pay invoices::

    >>> Payment = Model.get('account.payment')
    >>> pay_line = Wizard('account.move.line.pay', [line_to_pay, line_to_pay2])
    >>> pay_line.form.journal = payment_journal
    >>> pay_line.execute('start')
    >>> payments = Payment.find()
    >>> for payment in payments:
    ...     payment.click('approve')
    >>> len(payments)
    2

Process the payment::

    >>> PaymentGroup = Model.get('account.payment.group')
    >>> with config.set_context(active_ids=[p.id for p in payments]):
    ...     process_payment = Wizard('account.payment.process', payments)
    >>> process_payment.form.planned_date = None
    >>> process_payment.execute('process') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ('UserError', ('The field "Planned Date" on "Payment Group" is required.', ''))
    >>> process_payment.form.planned_date = datetime.datetime(2018, 6, 30)
    >>> process_payment.execute('process') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ('UserError', ('Must define B1 or B2+B3 or B4+B5 in Journal "Confirming" for confirming header record.', ''))

Add set fields in journal::

    >>> payment_journal.confirming_fields = {'codigo_operacion': 'PBA'}
    >>> payment_journal.save()
    >>> process_payment.execute('process') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ('UserError', ('Must define C1 in Journal "Confirming" for confirming header record.', ''))
    >>> payment_journal.confirming_fields = {'codigo_operacion': 'PBA', 'forma_pago': 'T'}
    >>> payment_journal.save()
    >>> process_payment.execute('process') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ('UserError', ('Must define B2 and B4 in Party "Party 1" for confirming detail record.', ''))

Add set fields in parties::

    >>> FieldSet = Model.get('account.payment.confirming.field.set')
    >>> Modeldata = Model.get('ir.model.data')
    >>> data, = Modeldata.find([
    ...     ('module', '=', 'account_payment_confirming_sfc'),
    ...     ('fs_id', '=', 'sfc53_set_party')])
    >>> values = party.confirming_values.new()
    >>> values.field_set = FieldSet(data.db_id)
    >>> values.confirming_fields = {
    ...     'tipo_identificador': '0',
    ...     'tipo_persona': 'J'}
    >>> party.save()
    >>> values = party2.confirming_values.new()
    >>> values.field_set = FieldSet(data.db_id)
    >>> values.confirming_fields = {
    ...     'tipo_identificador': '0',
    ...     'tipo_persona': 'F'}
    >>> party2.save()
    >>> process_payment.execute('process') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ('UserError', ('Must define B9 in Party address "Party 1" for confirming detail record.', ''))

Add set fields in party address::

    >>> data_addr, = Modeldata.find([
    ...     ('module', '=', 'account_payment_confirming_sfc'),
    ...     ('fs_id', '=', 'sfc53_set_address')])
    >>> address = party.addresses[0]
    >>> values = address.confirming_values.new()
    >>> values.field_set = FieldSet(data_addr.db_id)
    >>> values.confirming_fields = {'tipo_via': 'CL'}
    >>> party.save()
    >>> address = party2.addresses[0]
    >>> values = address.confirming_values.new()
    >>> values.field_set = FieldSet(data_addr.db_id)
    >>> values.confirming_fields = {'tipo_via': 'CL'}
    >>> party2.save()
    >>> process_payment.execute('process') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ('UserError', ('Must define B5, B6 and B7 in Party "John Smith Jackson" for confirming detail record.', ''))
    >>> values = party2.confirming_values[0]
    >>> values.confirming_fields = {
    ...     'tipo_identificador': '0',
    ...     'tipo_persona': 'F',
    ...     'apellido1': 'Smith',
    ...     'apellido2': 'Jackson',
    ...     'nombre': 'John'}
    >>> party2.save()
    >>> process_payment.execute('process')
    >>> group, = PaymentGroup.find()
    >>> message, = group.confirming_messages
    >>> file = os.path.join(os.path.dirname(__file__), 'sample.txt')
    >>> sample = io.open(file, 'rb').read().decode('ascii')

.. note:: parametrize group number due to in postgres is increased in each wizard error
    >>> message.message == (sample % {'groupnumber': group.number})
    True